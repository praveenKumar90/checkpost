import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetInfoComponent } from './get-info/get-info.component';
import { ShowInfoComponent } from './show-info/show-info.component';


const routes: Routes = [
  {path: "get-info", component: GetInfoComponent},
  {path: "show-info", component: ShowInfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
