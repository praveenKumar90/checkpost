import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-show-info',
  templateUrl: './show-info.component.html',
  styleUrls: ['./show-info.component.css']
})
export class ShowInfoComponent implements OnInit {

  data: any;
  constructor(private global: GlobalService) {
    this.data = global.ssoData;
   }

  ngOnInit() {
  }
}
