import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-info',
  templateUrl: './get-info.component.html',
  styleUrls: ['./get-info.component.css']
})
export class GetInfoComponent implements OnInit {
  ssoid: string = "NAVEEN.KUMAR4140";
  data: any;
  counter = 0;
  constructor(private global:GlobalService, private route: Router) { 
    
  }

  ngOnInit() {
  }

  handleInput(event: KeyboardEvent){
    if(event.keyCode == 13){
      this.check();
    }
  }

  check(){
    this.counter = 1;
    let me = this;
    var xhttp = new XMLHttpRequest();
    var url = "http://sso.rajasthan.gov.in:8888/SSOREST/GetUserDetailJSON/"+this.ssoid+"/Settlement.Test/Rajasthan@17";
    xhttp.open("GET", url, true);
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          // Typical action to be performed when the document is ready:
          me.global.ssoData = JSON.parse(xhttp.responseText);
          me.data = me.global.ssoData;
          me.counter = 0;
          // me.route.navigateByUrl('show-info');
        }
    };
    xhttp.send();
  }
}
